# Getting started with GitLab CI

## Resources
* [Documentation about GitLab CI](https://docs.gitlab.com/ce/ci/README.html)
* [Getting started documentation](https://docs.gitlab.com/ee/ci/quick_start/)
* [.gitlab-ci.yml file documentation](https://gitlab.com/help/ci/yaml/README.md)

## Steps
1. Follow steps from second link

### .gitlab-ci.yml file
* This .gitlab-ci.yml file is used in current project. It is a simple example of how maven project schould be checked.
Like you see pipeline has two jobs which are executed in different stages. Jobs are executed parallel.

```yml
image: maven:3-jdk-8
stages:
  - test
  - build
maven_test:
  stage: test
  script:   
    - echo "Testing project with maven"
    - mvn verify
  #tags: 
  #  - maven 
  #tags for choosing runner
maven_build:
  stage: build
  script:
     - "mvn package -B"
  artifacts:
    paths:
      - target/*.jar
```

* Maven image is being used because we execute maven commands.

```yml
image: maven:3-jdk-8
```

